package com.example.scg.route;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

/**
 * @author Aaron
 */
public interface RouteRepositoryOperations extends
    ReactiveMongoRepository<MongoRouteDefinition, String> {

  /**
   * 分页查询
   *
   * @param pageable 分页
   * @return 当前页
   */
  @Query(value = "{}", sort = "{_id:1}")
  Flux<MongoRouteDefinition> findAll(Pageable pageable);
}
