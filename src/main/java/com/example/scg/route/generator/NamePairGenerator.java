package com.example.scg.route.generator;

import javax.validation.constraints.NotNull;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.data.util.Pair;

/**
 * Name加NameValuePair参数类型的过滤器定义和断言定义（FilterDefinition&PredicateDefinition）生成器
 *
 * CreateTime: 2020-01-12
 *
 * @author HuangZhongHui
 **/
public class NamePairGenerator {

  /**
   * 生成路由过滤器定义
   *
   * @param name 过滤器名称
   * @param nameValue 名值对[name,value]
   * @return 路由过滤器定义
   */
  public static FilterDefinition createFilter(@NotNull String name,
      @NotNull Pair<String, String> nameValue) {
    FilterDefinition definition = new FilterDefinition();
    definition.setName(name);
    definition.addArg("name", nameValue.getFirst());
    definition.addArg("value", nameValue.getSecond());
    return definition;
  }

// ### 目前不存在这种形式的断言  ###
//  /**
//   * 生成断言定义
//   *
//   * @param name 断言名称
//   * @param nameValue 名值对[name,value]
//   * @return 断言定义
//   */
//  public static PredicateDefinition createPredicate(@NotNull String name,
//      @NotNull Pair<String, String> nameValue) {
//    PredicateDefinition definition = new PredicateDefinition();
//    definition.setName(name);
//    definition.addArg("name", nameValue.getFirst());
//    definition.addArg("value", nameValue.getSecond());
//    return definition;
//  }
}
