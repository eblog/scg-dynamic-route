package com.example.scg.route.generator.config;

import java.time.Duration;
import java.util.Map;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

/**
 * 重试的等待策略配置
 *
 * CreateTime: 2020-01-12
 *
 * @author HuangZhongHui
 **/
@Data
@Builder
public class BackoffConfig {

  private static final String PRE_FIX = "backoff.";
  private static final int MINI = 2;

  private Duration firstBackoff;
  private Duration maxBackoff;
  private int factor;
  private boolean basedOnPreviousValue;

  public void fillConfMap(@NotNull Map<String, String> args) {
    if (firstBackoff != null) {
      args.put(PRE_FIX + "firstBackoff", String.valueOf(firstBackoff.toMillis()));
    }
    if (maxBackoff != null) {
      args.put(PRE_FIX + "maxBackoff", String.valueOf(maxBackoff.toMillis()));
    }
    if (factor >= MINI) {
      args.put(PRE_FIX + "factor", String.valueOf(factor));
    }
    args.put(PRE_FIX + "basedOnPreviousValue", String.valueOf(basedOnPreviousValue));
  }
}
