package com.example.scg.route;

import org.springframework.beans.BeanUtils;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Aaron
 */
@Document("gwRoutes")
public class MongoRouteDefinition extends RouteDefinition {

  public static MongoRouteDefinition from(RouteDefinition route) {
    MongoRouteDefinition newRoute = new MongoRouteDefinition();
    BeanUtils.copyProperties(route, newRoute);
    return newRoute;
  }
}
