package com.example.scg.route;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import reactor.core.publisher.Mono;

@Slf4j
@SpringBootTest
class MongoRouteDefinitionRepositoryTest {

  public static final String UP_STREAM = "http://localhost:9999";

  @Autowired
  MongoRouteDefinitionRepository repository;

  @Test
  void save() throws InterruptedException {
    repository.save(Mono.just(newRoute(10001)))
        .subscribe(System.out::println);

    this.publishEvent();
  }

  @Test
  void saveAll() {
    this.deleteAll();
    int cycle = 10;
    while (cycle-- > 0) {
      int count = 1000;
      List<MongoRouteDefinition> routes = new ArrayList<>(count);
      while (count-- > 0) {
        routes.add(newRoute(cycle * 1000 + count));
      }
      repository.getRepositoryOperation().saveAll(routes).blockLast();
    }
  }

  @Test
  void findAll() {
  }

  @Test
  void deleteAll() {
    repository.getRepositoryOperation().deleteAll().block();
  }

  @Test
  void delete() {
    repository.delete(Mono.just("60c799b16fc37b32a34ac204"))
        .block();
  }

  @Test
  void publishEvent() throws InterruptedException {
    repository.publishEvent();
    TimeUnit.MINUTES.sleep(15);
  }

  private MongoRouteDefinition newRoute(int path) {
    MongoRouteDefinition route = new MongoRouteDefinition();
    route.setId(ObjectId.get().toHexString());
    PredicateDefinition predicate = new PredicateDefinition("Path=/mock/" + path);
    route.setPredicates(Collections.singletonList(predicate));
    List<FilterDefinition> filters = new LinkedList<>();
    filters.add(new FilterDefinition("AddRequestHeader=x-benchmark-routeId," + route.getId()));
    filters.add(new FilterDefinition("PrefixPath=/ok"));
    route.setFilters(filters);
    route.setOrder(1);
    route.setUri(URI.create(UP_STREAM));
    return route;
  }
}